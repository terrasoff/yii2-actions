<?php
/**
 * inline action for model saving
 *  - ajax model saving
 *  - save using captcha
 *
 * Author: terrasoff
 * Email: terrasoff@terrasoff.ru
 * Skype: tarasov.konstantin
 */

namespace terrasoff\yii2\actions;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * Class Save
 * @package terrasoff\yii2\actions
 * @property ActiveRecord $model
 * @property bool $hasCaptcha
 * @property string $captchaScenario
 * @property \Closure $onSuccess
 */
class Save extends \yii\base\Action
{
    /**
     * Full name of model's class
     * @var string
     */
    public $model = null;

    /**
     * If action has captcha checking
     * @var bool
     */
    public $hasCaptcha = false;

    /**
     * Name of captcha scenario to set captcha
     * @var string
     */
    public $captchaScenario = 'captcha';

    /**
     * Callback to prepare response in case of using ajax
     * @var \Closure
     */
    public $onSuccess = null;

    public function run()
    {
        $re = [];

        /** @var ActiveRecord $model  */
        $model = new $this->model();

        if ($this->hasCaptcha instanceof \Closure)
            $this->hasCaptcha = call_user_func($this->hasCaptcha, $this->model);
        if ($this->hasCaptcha)
            $model->setScenario($this->captchaScenario);
        $model->load(Yii::$app->request->post());

        if (!$model->save()) {
            $re['errors'] = $model->getErrors();
        } else {
            if ($this->onSuccess) {
                if ($this->onSuccess instanceof \Closure)
                    $re['result'] = call_user_func($this->onSuccess, $model);
            }
        }

        if (Yii::$app->request->isAjax)
            echo Json::encode($re);
        else
            $this->controller->redirect(Yii::$app->user->returnUrl);
    }
}