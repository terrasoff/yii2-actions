<?php
/**
 * inline action for simple list view
 * User: terrasoff
 * Date: 5/19/14 4:22 PM
 */

namespace terrasoff\yii2\actions;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;


/**
 * Class View
 * @package terrasoff\yii2\actions
 *
 * @property string $listView;
 * @property string $itemView;
 * @property string $modelView;
 * @property integer $pageSize;
 * @property string $idAttribute;
 * @property string $titleAttribute;
 * @property ActiveQuery $query;
 */
class View extends \yii\base\Action
{
    public $listView = 'items';
    public $itemView = 'item';
    public $modelView = 'model';
    public $pageSize = 10;
    public $query = null;

    public $idAttribute = null;
    public $titleAttribute = null;

    public function run()
    {
        // render specified model by id
        $id = (int)Yii::$app->request->getQueryParam($this->idAttribute);
        if ($this->idAttribute !== null && $id) {
            $model = $this->query->where([$this->idAttribute => $id])->one();
            if ($model !== null)
                return $this->controller->render($this->modelView,[
                    'model' => $model,
                ]);
        // render specified model by title
        } else {
            $id = Yii::$app->request->getQueryParam($this->titleAttribute);
            if ($this->titleAttribute !== null && $id) {
                $model = $this->query->where([$this->titleAttribute => $id])->one();
                if ($model !== null)
                    return $this->controller->render($this->modelView,[
                        'model' => $model,
                    ]);
            }
        }

        // render list
        return $this->controller->render($this->listView,[
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => $this->query,
                'pagination' => [
                    'pageSize' => $this->pageSize,
                ],
            ]),
            'itemView' => $this->itemView,
            'modelView' => $this->modelView,
        ]);
    }
}